import { Cmps383FrontendPage } from './app.po';

describe('cmps383-frontend App', () => {
  let page: Cmps383FrontendPage;

  beforeEach(() => {
    page = new Cmps383FrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
