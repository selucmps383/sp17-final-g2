///<reference path="Models/AccountModels.ts"/>
import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import {LoginModel, RegisterModel} from './Models/AccountModels';
import {AccountCalls} from './ApiCalls/AccountCalls';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isLoggedIn;
  isAdmin;
  constructor(private authServ: AccountCalls) {
    if (localStorage.getItem('token')) {
      this.isLoggedIn = true;
      this.isAdmin = localStorage.getItem('token')[0] === '0';
    } else {
      this.isLoggedIn = false;
    }
  }

  registerForSite(form: NgForm) {
    if ( form.value.registerPassword === form.value.register2ndPassword ) {
      const answer = new RegisterModel();
      answer.Email = form.value.registerUsername;
      answer.Password = form.value.registerPassword;
      form.resetForm();
      this.authServ.Register(answer);
    }
  }
  loginForSite(form: NgForm) {
    const answer = new LoginModel();
    answer.UserName = form.value.loginUsername;
    answer.Password = form.value.loginPassword;
    form.resetForm();
    this.authServ.Login(answer).subscribe(
      result => {
        if ( result.json().success = true ) {
          localStorage.setItem('token', result.json().token);
          if (result.json().token[0] === '0') {
            this.isAdmin = true;
          }
          this.isLoggedIn = true;
        } else {
          localStorage.clear();
          location.reload();
          this.isLoggedIn = false;
          this.isAdmin = false;
        }
      },
      error => {
        this.isLoggedIn = false;
      }
    );
  }
  logout() {
    localStorage.clear();
    location.reload();
    this.isLoggedIn = false;
    this.isAdmin = false;
  }
}

