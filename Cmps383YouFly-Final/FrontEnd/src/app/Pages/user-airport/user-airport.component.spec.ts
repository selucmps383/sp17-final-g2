import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAirportComponent } from './user-airport.component';

describe('UserAirportComponent', () => {
  let component: UserAirportComponent;
  let fixture: ComponentFixture<UserAirportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAirportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAirportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
