import { Component, OnInit } from '@angular/core';
import {AirportModel} from '../../../Models/AirPortModels';
import {AirPortCalls} from '../../../ApiCalls/AirPortCalls';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-admin-airport',
  templateUrl: './admin-airport.component.html',
  styleUrls: ['./admin-airport.component.css']
})
export class AdminAirportComponent implements OnInit {
  airportsList;
  filteredList = [];
  paginatedList = [];
  startNum = 0;
  endNum = 20;
  pages = [1];
  currentPage = 1;
  showEdit = false;
  currentEditAirport;
  showCreate = false;
  constructor( private airportCalls: AirPortCalls) {
  }

  ngOnInit() {
    this.loadAirports();
  }

  loadAirports() {
    this.airportCalls.GetListOfAllAirports().subscribe(
      result => {
        this.airportsList = result.json();
        this.filteredList = result.json();
        this.updatePage(0);
      },
      error => {
        console.log(error.json());
      }
    );
  }
  updatePage(pageNumber) {
    this.currentPage = pageNumber;
    this.startNum = ( pageNumber * 100 ) - 1;
    this.endNum = ( pageNumber * 100 ) + 100;
    this.applyPagination();
  }

  applyPagination() {
    this.paginatedList = [];
    let num = 0;
    this.filteredList.map(airport => {
        if ( num > this.startNum && num < this.endNum ) {
          this.paginatedList.push(airport);
        }
        num++;
    });
    const numOfPages = Math.ceil( this.filteredList.length / 100 );
    let i = 1;
    this.pages = [];
    while ( i < numOfPages) {
      this.pages.push(i);
      i++;
    }
  }

  filterByName(answer) {
      console.log('went into answer');
      this.filteredList = [];
      this.airportsList.map(airport => {
        if ( airport.name.toLowerCase().includes(answer.toLowerCase())) {
          this.filteredList.push(airport);
        }
        this.applyPagination();
      });
  }
  filterByCity(answer) {
      this.filteredList = [];
      this.airportsList.map(airport => {
        if ( airport.city.toLowerCase().includes(answer.toLowerCase())) {
          this.filteredList.push(airport);
        }
        this.applyPagination();
      });
  }
  filterByCountry(answer) {
      this.filteredList = [];
      this.airportsList.map(airport => {
        if ( airport.country.toLowerCase().includes(answer.toLowerCase())) {
          this.filteredList.push(airport);
        }
        this.applyPagination();
      });
  }
  filterByDomestic(answer) {
      this.filteredList = [];
      this.airportsList.map(airport => {
        if ( airport.domesticIcao.toLowerCase().includes(answer.toLowerCase()) ) {
          this.filteredList.push(airport);
        }
        this.applyPagination();
      });
  }
  filterByInternatiopnal(answer) {
      this.filteredList = [];
      this.airportsList.map(airport => {
        if ( airport.internationalIcao.toLowerCase().includes(answer.toLowerCase())) {
          this.filteredList.push(airport);
        }
        this.applyPagination();
      });
  }

  toggleEdit(id) {
    this.showCreate = false;
    this.airportCalls.FindAirportById(id).subscribe(
      result => {
        this.currentEditAirport = result.json();
        this.showEdit = true;
      },
      error => {
        console.log(error.json());
      }
    );
  }

  toggleCreate() {
    this.currentEditAirport = new AirportModel();
    this.showCreate = false;
    this.showEdit = true;

  }

  subMitEdit(form: NgForm) {
    const airport = new AirportModel();
    airport.Name = form.value.airPortName;
    airport.Country = form.value.country;
    airport.City = form.value.City;
    airport.DomesticIcao = form.value.DomesticIcao;
    airport.InternationalIcao = form.value.InternationalIcao;
    airport.Lattitude = form.value.lattitude;
    airport.Longitude = form.value.longitude;
    airport.Altitude = form.value.Altitude;
    airport.Timezone = form.value.timezone;
    if (!this.showCreate) {
      airport.Id = this.currentEditAirport.id;
      this.airportCalls.EditAirport(airport).subscribe(
        result => {
          this.loadAirports();
          this.showEdit = false;
        },
        error => {
          console.log(error.json());
          this.showEdit = false;
        }
      );
    } else {
      this.airportCalls.CreateAirport(airport).subscribe(
        result => {
          this.loadAirports();
          this.showEdit = false;
        },
        error => {
          console.log(error.json());
          this.showEdit = false;
        }
      );
    }

  }

  deleleEntry(id) {
    this.airportCalls.DeleteAirport(id).subscribe(
      result => {
        console.log(result.json());
        this.loadAirports();
      },
      error => {
        console.log(error.json());
      }
    );
  }
}
