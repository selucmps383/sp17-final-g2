import { Component, OnInit } from '@angular/core';
import { IMyOptions } from 'mydatepicker';
import { FlightCalls } from '../../../ApiCalls/FlightApiCalls';
import { FlightCreationModel } from '../../../Models/FlightModels';
import { AirPortCalls } from '../../../ApiCalls/AirPortCalls';

@Component({
  selector: 'app-admin-flight',
  templateUrl: './admin-flight.component.html',
  styleUrls: ['./admin-flight.component.css']
})
export class AdminFlightComponent implements OnInit {
  private myDatePickerOptions: IMyOptions = {
  dateFormat: 'dd.mm.yyyy',
  };

  flightsPages = [1];
  flightsStartNum = 0;
  flightEndNum = 20;
  flightList;
  flightFilteredList = [];
  flightPaginatedList = [];
  flightCurrentPage = 1;

  flightFilterDepartureLocation = '';
  flightFilterArrivalLocation = '';
  flightFilterStartDate = {date: {year: new Date().getFullYear(), month: new Date().getMonth(), day: new Date().getDate()}};
  flightFilterEndDate = {date: {year: new Date().getFullYear(), month: new Date().getMonth(), day: new Date().getDate()}};

  ModelFlightForCreateEdit = new FlightCreationModel();

  flightDepartureAirportName = '';
  flightArrivalAirportName = '';

  showMenuCreateEdit = false;
  menuAirportIsCreate = false;
  showMenuAirportSelect = false;


  airportPages = [1];
  airportsStartNum = 0;
  airportEndNum = 20;
  airportList;
  airportFilteredList = [];
  airportPaginatedList = [];
  airportCurrentPage = 1;

  airportFilterName = '';
  airportFilterCity = '';
  airportFilterCountry = '';
  airportFilterDomesticCode = '';
  airportFilterInternationalCode = '';

  departureAirport = null;
  arrivalAirport = null;
  toggleChooseArrivalAirport = false;
  toggleChooseDepartureAirport = false;



  constructor( private flightCalls: FlightCalls, private airportCalls: AirPortCalls) {
    if ( this.flightFilterStartDate.date.month === 12 ) {
      this.flightFilterEndDate.date.year = this.flightFilterStartDate.date.year + 1;
      this.flightFilterEndDate.date.month = 1;
    } else {
      this.flightFilterEndDate.date.month++;
    }
  }

  ngOnInit() {
    this.loadFlights();
  }

  loadFlights() {
    this.flightCalls.GetListOfAllFlights().subscribe(
      result => {
        this.flightList = result.json();
        this.flightFilteredList = result.json();
        this.updateFlightPage(0);
      },
      error => {
        console.log(error.json());
      }
    );
  }

  loadAirports(){
    this.airportCalls.GetListOfAllAirports().subscribe(
      result => {
        this.airportList = result.json();
        this.airportFilteredList = result.json();
        this.updateAirportPage(0);
      },
      error => {
        console.log(error.json());
      }
    );
  }

  updateFlightPage(pageNumber) {
    this.flightCurrentPage = pageNumber;
    this.flightsStartNum = ( pageNumber * 50 ) - 1;
    this.flightEndNum = ( pageNumber * 50 ) + 50;
    this.flightApplyPagination();
  }

  updateAirportPage(pageNumber){
    this.airportCurrentPage = pageNumber;
    this.airportsStartNum = ( pageNumber * 5 ) - 1;
    this.airportEndNum = ( pageNumber * 5 ) + 5;
    this.airportApplyPagination();
  }

  flightApplyPagination() {
    this.flightPaginatedList = [];
    let num = 0;
    this.flightFilteredList.map(airport => {
      if ( num > this.flightsStartNum && num < this.flightEndNum ) {
        this.flightPaginatedList.push(airport);
      }
      num++;
    });
    const numOfPages = Math.ceil( this.flightFilteredList.length / 10 );
    let i = 1;
    this.flightsPages = [];
    while ( i < numOfPages) {
      this.flightsPages.push(i);
      i++;
    }
  }

  airportApplyPagination() {
    this.airportPaginatedList = [];
    let num = 0;
    this.airportFilteredList.map(airport => {
      if ( num > this.airportsStartNum && num < this.airportEndNum ) {
        this.airportPaginatedList.push(airport);
      }
      num++;
    });
    const numOfPages = Math.ceil( this.airportFilteredList.length / 10 );
    let i = 1;
    this.airportPages = [];
    while ( i < numOfPages) {
      this.airportPages.push(i);
      i++;
    }
  }

  flightApplyFilter () {
    this.flightFilteredList = this.flightList;
    if (this.flightFilterStartDate && this.flightFilterEndDate) {
      const currList = [];
      this.flightFilteredList.map(result => {
        const ResultDepartDate = result.departureTime.split(/[-T]+/);
        const ResultArriveDate = result.departureTime.split(/[-T]+/);

        if ((this.flightFilterStartDate.date.year <= ResultDepartDate[0] &&
          this.flightFilterStartDate.date.month <= ResultDepartDate[1] &&
          this.flightFilterStartDate.date.day <= ResultDepartDate[2]) &&
          (this.flightFilterEndDate.date.year <= ResultArriveDate[0] &&
          this.flightFilterEndDate.date.month <= ResultArriveDate[1] &&
          this.flightFilterEndDate.date.day <= ResultArriveDate[2])) {
          currList.push(result);
        }
      });
      this.flightFilteredList = currList;
    }
    if (this.flightFilterDepartureLocation !== '') {
      const currList = [];
      this.flightFilteredList.map(result => {
        if (result.departAirport.name.toLowerCase().includes(this.flightFilterDepartureLocation.toLowerCase())) {
          currList.push(result);
        }
      });
      this.flightFilteredList = currList;
    }
    if (this.flightFilterArrivalLocation !== '') {
      const currList = [];
      this.flightFilteredList.map(result => {
        if (result.arrivalAirport.name.toLowerCase().includes(this.flightFilterArrivalLocation.toLowerCase())) {
          currList.push(result);
        }
      });
      this.flightFilteredList = currList;
    }
  }
  airportApplyFilters() {
    this.airportFilteredList = this.airportList;
    if (this.airportFilterName !== '') {
      const currList = [];
      this.airportFilteredList.map(result => {
          currList.push(result);
      });
      this.airportFilteredList = currList;
    }

    if (this.airportFilterCountry !== '') {
      const currList = [];
      this.airportFilteredList.map(result => {
        currList.push(result);
      });
      this.airportFilteredList = currList;
    }

    if (this.airportFilterCity !== '') {
      const currList = [];
      this.airportFilteredList.map(result => {
        currList.push(result);
      });
      this.airportFilteredList = currList;
    }

    if (this.airportFilterDomesticCode !== '') {
      const currList = [];
      this.airportFilteredList.map(result => {
        currList.push(result);
      });
      this.airportFilteredList = currList;
    }

    if (this.airportFilterInternationalCode !== '') {
      const currList = [];
      this.airportFilteredList.map(result => {
        currList.push(result);
      });
      this.airportFilteredList = currList;
    }

  }

  toggleCreate() {
    this.ModelFlightForCreateEdit = new FlightCreationModel();
    this.ModelFlightForCreateEdit.DepartureTime = 'year-month-dayThr:min:sec';
    this.ModelFlightForCreateEdit.ArrivalTime = 'year-month-dayThr:min:sec';
    this.menuAirportIsCreate = true;
    this.showMenuCreateEdit = true;
    this.loadAirports();
  }

  toggleEdit(id) {
    this.ModelFlightForCreateEdit.ArrivalAirportId = id.arrivalAirportId;
    this.ModelFlightForCreateEdit.ArrivalTime = id.arrivalTime;
    this.ModelFlightForCreateEdit.BusinssClassPrice = id.businessClassPrice;

    this.ModelFlightForCreateEdit.DepartureAirportId = id.departAirportId;
    this.ModelFlightForCreateEdit.DepartureTime = id.departureTime;
    this.ModelFlightForCreateEdit.FirstClassPrice = id.FirstClassPrice;
    this.ModelFlightForCreateEdit.Id = id.id;
    this.ModelFlightForCreateEdit.TravelTime = id.travelTime;

    this.ModelFlightForCreateEdit.FirstClassSeats = 0;

    this.ModelFlightForCreateEdit.BusinessSeats = 0;
    this.ModelFlightForCreateEdit.TravelDistance = 0;

    this.flightArrivalAirportName = id.arrivalAirport.name;
    this.flightDepartureAirportName = id.departAirport.name;

    this.menuAirportIsCreate = false;
    this.showMenuCreateEdit = true;
    this.loadAirports();
  }

  chooseArrivalAirport(){
    this.toggleChooseArrivalAirport = true;
  }
  chooseDepartureAirport(){
    this.toggleChooseDepartureAirport = true;
  }

  selectFlight(int, place) {
    if (this.toggleChooseDepartureAirport === true) {
      this.ModelFlightForCreateEdit.DepartureAirportId = int;
      this.flightDepartureAirportName = place;
    }
    if (this.toggleChooseArrivalAirport === true) {
      this.ModelFlightForCreateEdit.ArrivalAirportId = int;
      this.flightArrivalAirportName = place;
    }
    this.toggleChooseArrivalAirport = false;
    this.toggleChooseDepartureAirport = false;
  }

  submitCreateEditMenu () {
    console.log(this.ModelFlightForCreateEdit);
    if (this.menuAirportIsCreate === true) {
      console.log(this.ModelFlightForCreateEdit);
      this.flightCalls.CreateFlight(this.ModelFlightForCreateEdit).subscribe(
        result => {
          console.log(result.json());
          this.loadFlights();
        },
        error => {
          console.log(error.json());
        }
      );
    } else {
      this.flightCalls.EditFlight(this.ModelFlightForCreateEdit).subscribe(
        result => {
          console.log(result.json());
          this.loadFlights();
        },
        error => {
          console.log(error.json());
        }
      );
    }
    this.showMenuCreateEdit = false;
  }


}
