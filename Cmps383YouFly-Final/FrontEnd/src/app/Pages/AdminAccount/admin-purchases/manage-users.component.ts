import { Component, OnInit } from '@angular/core';
import {UserCalls} from "../../../ApiCalls/UserCalls";
import {PurchaseCalls} from "../../../ApiCalls/PurchaseCalls";
import {AirPortCalls} from "../../../ApiCalls/AirPortCalls";
import {FlightCalls} from "../../../ApiCalls/FlightApiCalls";
import {INT_TYPE} from "@angular/compiler/src/output/output_ast";

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.css']
})
export class ManageUSersComponent implements OnInit {


  flightList;
  userList;
  airportList;
  currentList;

  paginatedList = [];
  startNum = 0;
  endNum = 20;
  pages = [1];
  currentPage = 1;

  priceSearch = '';
  airportDepartureSearch = '';
  airportArrivalSearch = '';
  showBook = false;


  constructor(private userCalls: UserCalls, private airportCalls: AirPortCalls, private flightCalls: FlightCalls, private purchaseCalls: PurchaseCalls ) {
  }

  ngOnInit() {
    this.GetUsers();
    this.GetAirports();
    this.GetFlightList();
  }

  updatePage(pageNumber) {
    this.currentPage = pageNumber;
    this.startNum = ( pageNumber * 100 ) - 1;
    this.endNum = ( pageNumber * 100 ) + 100;
    this.applyPagination();
  }

  applyPagination() {
    this.paginatedList = [];
    let num = 0;
    this.currentList.map(flight => {
      if ( num > this.startNum && num < this.endNum ) {
        this.paginatedList.push(flight);
      }
      num++;
    });

    const numOfPages = Math.ceil( this.currentList.length / 100 );
    let i = 1;
    this.pages = [];
    while ( i < numOfPages) {
      this.pages.push(i);
      i++;
    }
  }

  GetUsers(){
    this.userList = [];
      this.userCalls.GetUserList().subscribe(
        result => {

           this.userList = result.json();

         },
         error => {}
        );

  }

  GetAirports(){
    this.airportList = [];
    this.airportCalls.GetListOfAllAirports().subscribe(
      result => {

        this.airportList = result.json();
      },
      error => {}
    );

  }

  GetFlightList(){
    this.flightList = [];
 //   this.flightCalls.GetListOfAllFlights().subscribe(
  //    result => {
  //      this.flightList = result.json();
   //     this.currentList = result.json();
   //     console.log(this.currentList);
   //   },
   //   error => {}
  //  );

    this.purchaseCalls.GetFlightData().subscribe(
      result => {
        this.flightList = result.json();
        this.currentList = result.json();

      },
      error => {}
    )
  }


  FilterPrice(){
    console.log(this.currentList);
    if (this.priceSearch !== ''){
      this.currentList = [];
      this.flightList.map(obj => {
      // if( obj.email.toString().toLowerCase().includes(this.emailSearch.toString().toLowerCase()) ) {
      //    this.currentList.push(obj.email);
          if (obj.businessClassPrice <= parseInt(this.priceSearch) || obj.firstClassPrice <= parseInt(this.priceSearch) ) {
            this.currentList.push(obj);
       }
      });
    }
  }

  GetPurchasesByArrivalAirport(){
      if (this.airportArrivalSearch !== ''){
        this.currentList = [];
        this.flightList.map(obj => {
          if( obj.arrivalAirport.name.toString().toLowerCase().includes(this.airportArrivalSearch.toString().toLowerCase()) ) {
           this.currentList.push(obj);

            console.log(this.currentList);
          }
        });
      }
    }

  GetPurchasesByDepartureAirport(){
    if (this.airportDepartureSearch !== ''){
      this.currentList = [];
      this.flightList.map(obj => {
        if( obj.departAirport.name.toString().toLowerCase().includes(this.airportDepartureSearch.toString().toLowerCase()) ) {
          this.currentList.push(obj);
        }
      });
    }
  }


  BookFlight(id){
    this.airportCalls.FindAirportById(id).subscribe(
      result => {
        this.showBook = true;
      },
      error => {
        console.log(error.json());
      }
    );
  }

}


