import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from './authService';

@Injectable()
export class UserRouteGuard implements CanActivate {

  constructor(private authService: AuthService) {}

  canActivate() {
    if ( this.authService.isUser() || this.authService.isAdmin() ) {
      return true;
    }
  }
}
