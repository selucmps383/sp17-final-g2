import { Injectable } from '@angular/core';
import {LoginModel, RegisterModel} from '../Models/AccountModels';

@Injectable()
export class AuthService {

  isUser() {
    const token = localStorage.getItem('token');
    if (token !== null ) {
      return token[0] === '0';
    }
    return false;
  }
  isAdmin() {
    const token = localStorage.getItem('token');
    if (token !== null ) {
      return token[0] === '1';
    }
    return false;
  }
}
