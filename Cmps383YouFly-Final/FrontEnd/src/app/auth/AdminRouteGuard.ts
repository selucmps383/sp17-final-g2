import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from './authService';

@Injectable()
export class AdminRouteGuard implements CanActivate {

  constructor(private authService: AuthService) {}

  canActivate() {
    return this.authService.isAdmin();
  }
}
