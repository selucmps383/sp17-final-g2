"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
///<reference path="Models/AccountModels.ts"/>
var core_1 = require("@angular/core");
var AccountModels_1 = require("./Models/AccountModels");
var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app works!';
    }
    AppComponent.prototype.registerForSite = function (form) {
        if (form.value.registerPassword === form.value.register2ndPassword) {
            console.log(form.value);
            var answer = new AccountModels_1.RegisterModel();
            answer.Email = form.value.registerUsername;
            answer.Password = form.value.registerPassword;
            form.resetForm();
        }
    };
    AppComponent.prototype.loginForSite = function (form) {
        var anwer = new AccountModels_1.LoginModel();
        anwer.UserName = form.value.loginUsername;
        anwer.Password = form.value.loginPassword;
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'app-root',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.css']
    })
], AppComponent);
exports.AppComponent = AppComponent;
