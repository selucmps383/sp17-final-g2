import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule} from 'mydatepicker';

import { HttpModule } from '@angular/http';
import { AccountCalls } from './ApiCalls/AccountCalls';
import { AirPortCalls } from './ApiCalls/AirPortCalls';
import { UserCalls } from './ApiCalls/UserCalls';
import {PurchaseCalls} from "./ApiCalls/PurchaseCalls";


import { AppComponent } from './app.component';
import {RouterModule, Routes } from '@angular/router';
import {UserRouteGuard} from './auth/UserRouteGuard';
import {AdminRouteGuard} from './auth/AdminRouteGuard';
import {AuthService} from './auth/authService';

import { HomeComponent } from './Pages/home/home.component';
import { AdminFlightComponent } from './Pages/AdminAccount/admin-flight/admin-flight.component';
import { UserFlightComponent } from './Pages/user-flight/user-flight.component';
import { UserAirportComponent } from './Pages/user-airport/user-airport.component';
import { AdminAirportComponent } from './Pages/AdminAccount/admin-airport/admin-airport.component';
import { ManageUSersComponent } from './Pages/AdminAccount/admin-purchases/manage-users.component';
import {FlightCalls} from "./ApiCalls/FlightApiCalls";


const appRoutes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'adminAirport', component: AdminAirportComponent, canActivate: [AdminRouteGuard]},
  { path: 'adminFlight', component: AdminFlightComponent, canActivate: [AdminRouteGuard]},
  { path: 'Airport', component: UserAirportComponent, canActivate: [UserRouteGuard]},
  { path: 'Flight', component: ManageUSersComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AdminFlightComponent,
    UserFlightComponent,
    UserAirportComponent,
    AdminAirportComponent,
    ManageUSersComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MyDatePickerModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AccountCalls,
    AirPortCalls,
    FlightCalls,
    UserCalls,
    PurchaseCalls,
    AuthService,
    UserRouteGuard,
    AdminRouteGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
