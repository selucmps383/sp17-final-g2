"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var AccountCalls_1 = require("./ApiCalls/AccountCalls");
var app_component_1 = require("./app.component");
var router_1 = require("@angular/router");
var home_component_1 = require("./Pages/home/home.component");
var content_page_component_1 = require("./Pages/content-page/content-page.component");
var UserRouteGuard_1 = require("./auth/UserRouteGuard");
var AdminRouteGuard_1 = require("./auth/AdminRouteGuard");
var appRoutes = [
    { path: '', component: home_component_1.HomeComponent, canActivate: [UserRouteGuard_1.UserRouteGuard] },
    { path: 'content', component: content_page_component_1.ContentPageComponent, canActivate: [AdminRouteGuard_1.AdminRouteGuard] }
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        declarations: [
            app_component_1.AppComponent,
            home_component_1.HomeComponent,
            content_page_component_1.ContentPageComponent
        ],
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            router_1.RouterModule.forRoot(appRoutes)
        ],
        providers: [
            AccountCalls_1.AccountCalls,
        ],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
