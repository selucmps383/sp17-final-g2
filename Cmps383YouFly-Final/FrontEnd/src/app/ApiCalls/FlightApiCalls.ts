import {Injectable} from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {TokenModel} from '../Models/AccountModels';
import {FindByNumberModel} from '../Models/BaseModels';
import {FlightCreationModel, } from '../Models/FlightModels';

 const url = 'http://youflyapiapp.azurewebsites.net/api/Flight';
// const url = 'http://localhost:55520/api/Flight';

@Injectable()
export class FlightCalls {
  constructor ( private http: Http ) {
  }

  GetListOfAllFlights() {
    const route = '/GetList';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    const content = new TokenModel();
    content.SessionToken = localStorage.getItem('token');
    return this.http.post(url + route, content, options);
  }

  FindFlightById (flight: number) {
    const route = '/FindById';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    const content = new FindByNumberModel();
    content.Id = flight;
    content.SessionToken = localStorage.getItem('token');
    return this.http.post(url + route, content, options);
  }

  CreateFlight(flight: FlightCreationModel) {
    const route = '/Create';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    flight.SessionToken = localStorage.getItem('token');
    console.log(flight);
    return this.http.post(url + route, flight, options);
  }

  EditFlight (flight: FlightCreationModel) {
    const route = '/Edit';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    flight.SessionToken = localStorage.getItem('token');
    return this.http.post(url + route, flight, options);
  }

  DeleteFlight (flight: number) {
    const route = '/Delete';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    const content = new FindByNumberModel();
    content.Id = flight;
    content.SessionToken = localStorage.getItem('token');
    return this.http.post(url + route, content, options);
  }
}

