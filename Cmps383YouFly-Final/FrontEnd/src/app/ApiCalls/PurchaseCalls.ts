/**
 * Created by Clinton on 4/29/2017.
 */

import {Injectable} from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {AirportModel} from '../Models/AirPortModels';
import {TokenModel} from '../Models/AccountModels';

const url = 'http://youflyapiapp.azurewebsites.net/api/users';
const flightUrl = 'http://youflyapiapp.azurewebsites.net/api/tester/getflights';
//const url = 'http://localhost:64375//api/users';

@Injectable()
export class PurchaseCalls {
  constructor(private http: Http) {
  }

  GetUserByEmail(email) {

    const route = '/GetUserByEmail';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    const content = {SessionToken: localStorage.getItem('token'), Content: email};

    return this.http.post(url + route, content, options);
  }

  GetFlightData(){
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    return this.http.get(flightUrl);

  }



}
