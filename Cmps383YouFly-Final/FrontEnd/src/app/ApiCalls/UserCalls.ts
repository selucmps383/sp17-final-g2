/**
 * Created by Clinton on 4/29/2017.
 */

import {Injectable} from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {AirportModel} from '../Models/AirPortModels';
import {TokenModel} from '../Models/AccountModels';

const url = 'http://youflyapiapp.azurewebsites.net/api/users';
//const url = 'http://localhost:64375//api/users';

@Injectable()
export class UserCalls {
  constructor(private http: Http) {
  }

  GetUserList() {
    const route = '/getusers';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    const content = new TokenModel();
    content.SessionToken = localStorage.getItem('token');
    return this.http.post(url + route, content, options);
  }

}
