import {Injectable} from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {LoginModel, RegisterModel, TokenModel} from '../Models/AccountModels';

 const url = 'http://youflyapiapp.azurewebsites.net';
// const url = 'http://localhost:56766';

@Injectable()
export class AccountCalls {
  constructor ( private http: Http ) {
  }

  Register(content: RegisterModel) {
    const route = '/api/register';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    this.http.post(url + route, content, options).subscribe(
      result => {
        console.log(result);
        if ( result.json().success = true ) {
          return true;
        } else {
          return true;
        }
      },
      error => {
        return false;
      }
    );
  }

  Login(content: LoginModel) {
    const route = '/api/login';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    return this.http.post(url + route, content, options);
  }
}

