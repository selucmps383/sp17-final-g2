import {Injectable} from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {AirportModel} from '../Models/AirPortModels';
import {TokenModel} from '../Models/AccountModels';

 const url = 'http://youflyapiapp.azurewebsites.net/api/Airport';
// const url = 'http://localhost:55520//api/Airport';

@Injectable()
export class AirPortCalls {
  constructor ( private http: Http ) {
  }


  GetListOfAllAirports() {
    const route = '/getlist';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    const content = new TokenModel();
    content.SessionToken = localStorage.getItem('token');
    return this.http.post(url + route, content, options);
  }

  CreateAirport(airport: AirportModel) {
    const route = '/create';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    airport.SessionToken = localStorage.getItem('token');
    const content = JSON.parse(JSON.stringify(airport));
    return this.http.post(url + route, content, options);
  }
  EditAirport(airport: AirportModel) {

    const route = '/Edit';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    airport.SessionToken = localStorage.getItem('token');
    const content = JSON.parse(JSON.stringify(airport));
    return this.http.post(url + route, content, options);
  }
  FindAirportById(airport: number) {
    const route = '/FindById';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    const content = {SessionToken: localStorage.getItem('token'), Id: airport};
    return this.http.post(url + route, content, options);
  }
  FindAirportByName(airport: number) {
    const route = '/FindByName';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    const content = {SessionToken: localStorage.getItem('token'), Content: airport};
    return this.http.post(url + route, content, options);
  }
  FindAirportByCity(airport: string) {
    const route = '/FindByCity';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    const content = {SessionToken: localStorage.getItem('token'), Content: airport};
    return this.http.post(url + route, content, options);
  }
  GetAirportListByCountry(airport: string) {
    const route = '/FindByCountry';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    const content = {SessionToken: localStorage.getItem('token'), Content: airport};
    return this.http.post(url + route, content, options);
  }

  FindAirportByDomesticIcao(airport: string) {
    const route = '/FindByDomestic';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    const content = {SessionToken: localStorage.getItem('token'), Content: airport};
    return this.http.post(url + route, content, options);
  }

  FindEntryByInternationalIcao(airport: string) {
    const route = '/FindByInternational';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    const content = {SessionToken: localStorage.getItem('token'), Content: airport};
    return this.http.post(url + route, content, options);
  }

  DeleteAirport(airport: number) {
    const route = '/Delete';
    const headers = new Headers({
      'Content-Type': 'application/json',
    });
    const options = new RequestOptions({
      headers: headers
    });
    const content = {SessionToken: localStorage.getItem('token'), Content: airport};
    return this.http.post(url + route, content, options);
  }
}

