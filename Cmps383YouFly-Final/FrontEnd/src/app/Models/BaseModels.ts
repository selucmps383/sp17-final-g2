import {TokenModel} from './AccountModels';

export class FindByStringModel extends TokenModel {
  Content: string;
}

export class FindByNumberModel extends TokenModel {
  Id: number;
}
