import {TokenModel} from './AccountModels';

export class FlightCreationModel extends TokenModel {
  Id: number;
  Name: string;
  FirstClassPrice: number;
  BusinssClassPrice: number;
  DepartureAirportId: number;
  ArrivalAirportId: number;
  DepartureTime: string;
  ArrivalTime: string;
  TravelTime: number;
  TravelDistance: number;
  FirstClassSeats: number;
  BusinessSeats: number;
}
