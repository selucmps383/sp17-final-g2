import {TokenModel} from './AccountModels';


export class AirportModel extends TokenModel {
  Id: number;
  Name: string;
  City: string;
  Country: string;
  DomesticIcao: string;
  InternationalIcao: string;
  Lattitude: number;
  Longitude: number;
  Altitude: number;
  Timezone: number;
}
