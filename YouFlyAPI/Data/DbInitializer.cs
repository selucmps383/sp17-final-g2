﻿
using System;
using System.Linq;
using YouFlyAPI.Data.DataAccessClasses;
using YouFlyAPI.Data.ViewModels.AccountViewModels;
using YouFlyAPI.Models;
using YouFlyAPI.Models.AccountModels;
using YouFlyAPI.Tools;

namespace YouFlyAPI.Data
{//begin namespace

    public class DbInitializer
    {//begin class

        private AccountDataAccess _account;

        public DbInitializer(AccountDataAccess newAccount)
        {//begin constructor
            _account = newAccount;
        }//end constructor

        public void Initialize(DataContext context)
        {//begin method
            context.Database.EnsureCreated();
            if (!context.Airports.Any())
            {//begin if
                var reader = new TextReader();
                var array = reader.GetTextArray();
                foreach (var t in array)
                {//begin for
                    try
                    {//begin try
                        var a = t.Split(',');
                        var air = new Airport
                        {
                            Name = CleanString(a[1]),
                            City = CleanString(a[2]),
                            Country = CleanString(a[3]),
                            DomesticIcao = CleanString(a[4]),
                            InternationalIcao = CleanString(a[5]),
                            Lattitude = Convert.ToDouble(a[6]),
                            Longitude = Convert.ToDouble(a[7]),
                            Altitude = Convert.ToDouble(a[8]),
                            Timezone = Convert.ToInt32(a[9])
                        };
                        context.Airports.Add(air);
                        context.SaveChanges();
                        System.Threading.Thread.Sleep(100);
                    } //end try
                    catch (Exception)
                    {//begin catch
                        //ignore error
                    } //end catch
                }//end for
            }//end if
            context.SaveChanges();
            if (!context.Roles.Any())
            {//begin if
                var roles = new[]
                {//begin initialize
                    new RoleModel
                    {
                        Rank = 1,
                        Description = "Admin",
                        CreatedDateTime = DateTime.Now,
                        LastEditedDateTime = DateTime.Now,
                        IsActive = true
                    },
                    new RoleModel
                    {
                        Rank = 0,
                        Description = "User",
                        CreatedDateTime = DateTime.Now,
                        LastEditedDateTime = DateTime.Now,
                        IsActive = true
                    }
                };
                foreach (RoleModel role in roles)
                {//begin for
                    context.Roles.Add(role);
                }//end for
            }//end if
            context.SaveChanges();
            if(!context.Users.Any())
            {//begin if
                var admin = new RegisterViewModel();
                admin.Email = "admin@selu.edu";
                admin.Password = "password";
                _account.CreateNewUser(admin);
                System.Threading.Thread.Sleep(100);

                var mike = new RegisterViewModel();
                mike.Email = "michael.hutto@selu.edu";
                mike.Password = "password";
                _account.CreateNewUser(mike);
                System.Threading.Thread.Sleep(100);

                var clinton = new RegisterViewModel();
                clinton.Email = "clinton.walker@selu.edu";
                clinton.Password = "password";
                _account.CreateNewUser(clinton);
                System.Threading.Thread.Sleep(100);

                var aabi = new RegisterViewModel();
                aabi.Email = "aabishkar.timalsina@selu.edu";
                aabi.Password = "password";
                _account.CreateNewUser(aabi);
                System.Threading.Thread.Sleep(100);

                var robert = new RegisterViewModel();
                robert.Email = "robert.waguespack@selu.edu";
                robert.Password = "password";
                _account.CreateNewUser(robert);
                System.Threading.Thread.Sleep(100);

                var james = new RegisterViewModel();
                james.Email = "james.wallace-3@selu.edu";
                james.Password = "password";
                _account.CreateNewUser(james);
                System.Threading.Thread.Sleep(100);

                var josh = new RegisterViewModel();
                josh.Email = "jawetzel615@gmail.com";
                josh.Password = "password";
                _account.CreateNewUser(josh);
                System.Threading.Thread.Sleep(100);

                var gdoc = new RegisterViewModel();
                gdoc.Email = "ghassan.alkadi@selu.edu";
                gdoc.Password = "password";
                _account.CreateNewUser(gdoc);
                System.Threading.Thread.Sleep(100);
            }//end if
        }//end method

        private string CleanString(string newString)
        {//begin method
            char[] cleanList = new char[1];
            cleanList[0] = '"';
            var trimFront = newString.TrimStart(cleanList);
            var returnString = trimFront.TrimEnd(cleanList);
           return returnString;
        }//end method

    }//end class

}//end namespace