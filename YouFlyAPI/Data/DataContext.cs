﻿using YouFlyAPI.Models.AccountModels;
using Microsoft.EntityFrameworkCore;
using YouFlyAPI.Models;

namespace YouFlyAPI.Data
{//begin namespace

    public class DataContext : DbContext
    {//begin class

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {//begin constructor
        }//end constructor

        public DbSet<UserModel> Users { get; set; }
        public DbSet<SessionModel> Sessions { get; set; }
        public DbSet<RoleModel> Roles { get; set; }
        public DbSet<Airport> Airports { get; set; }
        public DbSet<Flight> Flights { get; set; }
        public DbSet<Seat> Seats { get; set; }

    }//end class

}//end namespace