﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using YouFlyAPI.Data.ViewModels.AccountViewModels;
using YouFlyAPI.Models.AccountModels;

namespace YouFlyAPI.Data.DataAccessClasses
{//begin namespace

    public class AccountDataAccess
    {//begin class

        private readonly DataContext _context;

        public AccountDataAccess(DataContext context)
        {//begin constructor
            _context = context;
        }//end constructor

        public bool CheckSessionToken(string token)
        {//begin method
            try
            {//begin try
                SessionModel oldToken = GetSessionByToken(token);
                if (oldToken.ExpireDateTime >= DateTime.Now)
                {//begin if
                    return UpdateSessionToken(oldToken);
                }//end if
                return false;
            }//end try
            catch (Exception e)
            {//begin catch
                return false;
            }//end catch
        }//end method

        public SessionModel CreaseSessionToken(LoginViewModel user)
        {//begin method
            var session = new SessionModel();
            session.ExpireDateTime = DateTime.Now.AddHours(8);
            session.LastEditedDateTime = DateTime.Now;
            session.CreatedDateTime = DateTime.Now;
            session.IsActive = true;
            session.Token = DateTime.Now.ToString(CultureInfo.InvariantCulture) + Guid.NewGuid().ToString() + Guid.NewGuid().ToString();
            try
            {//begin try
                session.User = GetUserByEmail(user.UserName);
                var role = GetRoleById(session.User.RoleId);
                session.Token = role.Rank + session.Token;
                session.UserId = session.User.Id;
                _context.Sessions.Add(session);
                _context.SaveChanges();
                return session;
            }//end try
            catch (Exception e)
            {//begin catch
                return null;
            }//end catch
        }//end method

        public bool UpdateSessionToken(SessionModel session)
        {//begin method
            try
            {//begin try
                var foundSession = GetSessionByToken(session.Token);
                foundSession.ExpireDateTime = DateTime.Now.AddHours(8);
                _context.Update(foundSession);
                _context.SaveChanges();
                return true;
            }//end try
            catch (Exception e)
            {//begin catch
                return false;
            }//end catch
        }//end method

        public bool CreateNewUser(RegisterViewModel user)
        {//begin method
            var role = GetRoleByDescription("User");
            UserModel userModel = new UserModel
            {//begin define
                Role = role,
                RoleId = role.Id,
                CreatedDateTime = DateTime.Now,
                LastEditedDateTime = DateTime.Now,
                IsActive = true,
                VerifiedEmail = false,
                Email = user.Email
            };//end define
            userModel = CreateCryptoForPassword(user.Password, userModel);
            try
            {//begin try
                _context.Users.Add(userModel);
                _context.SaveChanges();
                return true;
            }//end try
            catch (Exception e)
            {//begin catch
                return false;
            }//end catch
        }//end method

        public bool UpdateUser(UpdateUserViewModel user)
        {//begin method
            var changed = false;
            UserModel oldUser;
            try
            {//begin try
                oldUser = GetSessionByToken(user.SessionToken).User;
            }//end try
            catch (Exception e)
            {//begin catch
                return false;
            }//end catch
            if (!string.Equals(oldUser.Role.Description, user.Role) && user.Role.Length > 0)
            {//begin if
                oldUser.Role = GetRoleByDescription(user.Role);
                oldUser.RoleId = oldUser.Role.Id;
                changed = true;
            }//end if
            if (!string.Equals(oldUser.Email, user.Email) & user.Email.Length > 0)
            {//begin if
                oldUser.Email = user.Email;
                changed = true;
            }//end if
            if (!string.Equals(oldUser.Password, GetCrypotdPassword(user.UpdatedPassword, oldUser.Salt)) & user.UpdatedPassword.Length > 0)
            {//begin if
                oldUser.Password = GetCrypotdPassword(user.UpdatedPassword, oldUser.Salt);
                changed = true;
            }//end if
            if (changed)
            {//begin if
                try
                {//begin try
                    _context.Update(oldUser);
                    _context.SaveChanges();
                    return true;
                }//end try
                catch (Exception e)
                {//begin catch
                    return false;
                }//end catch
            }//end if
            return false;
        }//end method

        public RoleModel GetRoleByDescription(string descr)
        {//begin method
            return _context.Roles.Single(role => role.Description == descr);
        }//end method

        public SessionModel GetSessionByToken(string token)
        {//begin method
            return _context.Sessions.Single(session => string.Equals(session.Token, token));
        }//end method

        public UserModel GetUserByEmail(string email)
        {//begin method
            return _context.Users.Single(user => string.Equals(user.Email, email));
        }//end method

        public string GetCrypotdPassword(string password, byte[] salt)
        {//begin method
            return Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
        }//end method

        public UserModel CreateCryptoForPassword(string password, UserModel user)
        {//begin method
            var salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {//begin define
                rng.GetBytes(salt);
            }//end define
            Console.WriteLine($"Salt: {Convert.ToBase64String(salt)}");
            user.Salt = salt;
            user.Password = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
            return user;
        }//end method

        public RoleModel GetRoleById(int input)
        {//begin method
            return _context.Roles.Where(role => role.Id == input).ToList().First();
        }//end method

    }//end class

}//end namespace