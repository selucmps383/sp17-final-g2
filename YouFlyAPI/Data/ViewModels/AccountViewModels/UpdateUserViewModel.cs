﻿using System.ComponentModel.DataAnnotations;

namespace YouFlyAPI.Data.ViewModels.AccountViewModels
{//begin namespace

    public class UpdateUserViewModel : BaseViewModel
    {//begin class

        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public string UpdatedPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public string Role { get; set; }

    }//end class

}//end namespace