﻿using System.ComponentModel.DataAnnotations;

namespace YouFlyAPI.Data.ViewModels.AccountViewModels
{//begin namespace

    public class RegisterViewModel
    {//begin class

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }

    }//end class

}//end namespace