﻿using System.ComponentModel.DataAnnotations;

namespace YouFlyAPI.Data.ViewModels.AccountViewModels
{//begin namespace

    public class LoginViewModel
    {//begin class

        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }

    }//end class

}//end namespace