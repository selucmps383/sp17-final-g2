﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouFlyAPI.Data.ViewModels
{
    public class FindByStringViewModel : BaseViewModel
    {
        public string Content { get; set; }

    }
}
