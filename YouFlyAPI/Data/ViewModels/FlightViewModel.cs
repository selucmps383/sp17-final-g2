﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using YouFlyAPI.Models;

namespace YouFlyAPI.Data.ViewModels
{
    public class FlightViewModel : BaseViewModel
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public double FirstClassPrice { get; set; }
        public double BusinessClasPrice { get; set; }
        public DateTime DepartureTime { get; set; }
        public DateTime ArrivalTime { get; set; }
        public double TravelTime { get; set; }

        public ICollection<Seat> Seats { get; set; }

        [ForeignKey("DepartAirport")]
        public int? DepartAirportId { get; set; }
        public Airport DepartAirport { get; set; }

        [ForeignKey("ArrivalAirport")]
        public int? ArrivalAirportId { get; set; }
        public Airport ArrivalAirport { get; set; }


    }
}
