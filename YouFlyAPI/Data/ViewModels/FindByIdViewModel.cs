﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouFlyAPI.Data.ViewModels
{
    public class FindByIdViewModel : BaseViewModel
    {

        public int Id { get; set; }

    }
}
