﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouFlyAPI.Data.ViewModels
{
    public class AirportViewModel : BaseViewModel
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string DomesticIcao { get; set; }
        public string InternationalIcao { get; set; }
        public double Lattitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
        public int Timezone { get; set; }

    }
}
