﻿using System.ComponentModel.DataAnnotations;

namespace YouFlyAPI.Data.ViewModels
{//begin namespace

    public class BaseViewModel
    {//begin class

        [Required]
        public string SessionToken { get; set; }

    }//end class

}//begin namespace