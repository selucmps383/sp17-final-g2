﻿using System;
using System.ComponentModel.DataAnnotations;

namespace YouFlyAPI.Data
{//begin namespace

    public class BaseDataModel
    {//begin class

        [Key]
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime LastEditedDateTime { get; set; }

    }//end class

}//end namespace