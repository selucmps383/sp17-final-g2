﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace YouFlyAPI.Controllers
{//begin namespace

    [Route("api/[controller]")]
    public class ValuesController : Controller
    {//begin class

        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {//begin method
            return new string[] { "value1", "value2" };
        }//end method

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {//begin method
            return "value";
        }//end method

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {//begin method
            //todo stuff
        }//end method

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {//begin method
            //todo stuff
        }//end method

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {//begin method
            //todo stuff
        }//end method

    }//end class

}//end namespace