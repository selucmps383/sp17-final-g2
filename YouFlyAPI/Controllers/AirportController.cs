﻿
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using YouFlyAPI.Data;
using YouFlyAPI.Data.DataAccessClasses;
using YouFlyAPI.Data.ViewModels;
using YouFlyAPI.Models;

namespace YouFlyAPI.Controllers
{//begin namespace

    //[Produces("application/json")]
    [Route("api/Airport")]
    public class AirportDataController : Controller
    {//begin class

        private readonly AccountDataAccess _account;
        private readonly DataContext _db;

        public AirportDataController(DataContext context, AccountDataAccess newAccount)
        {//begin constructor
            _db = context;
            _account = newAccount;
        }//end constructor

        [HttpPost]
        [Route("getlist")]
        public JsonResult GetAirportList([FromBody] BaseViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if(tokenCheck)
            {//begin if
            var airportList = from airport in _db.Airports
                    select new Airport
                    {//begin define
                        Id = airport.Id,
                        Name = airport.Name,
                        City = airport.City,
                        Country = airport.Country,
                        DomesticIcao = airport.DomesticIcao,
                        InternationalIcao = airport.InternationalIcao,
                        Lattitude = airport.Lattitude,
                        Longitude = airport.Longitude,
                        Altitude = airport.Altitude,
                        Timezone = airport.Timezone,
                    }; //end define
                return Json(airportList.ToList());
            }//end if
            return Json(new { success = false });
        }//end method

        [HttpPost]
        [Route("create")]
        public JsonResult CreateAirport([FromBody] AirportViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if(tokenCheck)
            {//begin if
                var airport = new Airport();
                airport.Name = token.Name;
                airport.City = token.City;
                airport.Country = token.Country;
                airport.DomesticIcao = token.DomesticIcao;
                airport.InternationalIcao = token.InternationalIcao;
                airport.Lattitude = token.Lattitude;
                airport.Longitude = token.Longitude;
                airport.Altitude = token.Altitude;
                airport.Timezone = token.Timezone;
                _db.Airports.Add(airport);
                _db.SaveChanges();
                return Json(new {success = true});
            }//end if
            return Json(new { success = false });
        }//end method

        [HttpPost]
        [Route("Edit")]
        public JsonResult PostChangesForEdit([FromBody] AirportViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var airport = _db.Airports.First(result => result.Id.Equals(token.Id));
                airport.Name = token.Name;
                airport.City = token.City;
                airport.Country = token.Country;
                airport.DomesticIcao = token.DomesticIcao;
                airport.InternationalIcao = token.InternationalIcao;
                airport.Lattitude = token.Lattitude;
                airport.Longitude = token.Longitude;
                airport.Altitude = token.Altitude;
                airport.Timezone = token.Timezone;
                _db.Entry(airport).State = EntityState.Modified;
                _db.SaveChanges();
                return Json(new {success = true});
            }//end if
            return Json(new {success = false});
        }//end method

        [HttpPost]
        [Route("FindById")]
        public JsonResult FindEntryById([FromBody] FindByIdViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var foundAirportData = _db.Airports.Find(token.Id);
                var airport = new Airport
                {//begin define
                    Id = foundAirportData.Id,
                    Name = foundAirportData.Name,
                    City = foundAirportData.City,
                    Country = foundAirportData.Country,
                    DomesticIcao = foundAirportData.Country,
                    InternationalIcao = foundAirportData.InternationalIcao,
                    Lattitude = foundAirportData.Lattitude,
                    Longitude = foundAirportData.Longitude,
                    Altitude = foundAirportData.Altitude,
                    Timezone = foundAirportData.Timezone,
                }; //end define
                return Json(airport);
            }//end if
            return Json(new {success = false});
        }//end method

        [HttpPost]
        [Route("FindByName")]
        public JsonResult FindEntryByName([FromBody] FindByStringViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var returnObject = _db.Airports
                    .Where(result => result.Name.Contains(token.Content))
                    .ToList();
                return Json(returnObject);
            }//end if
            return Json(new {success = false});
        }//end method

        [HttpPost]
        [Route("FindByCity")]
        public JsonResult FindEntryByCity([FromBody] FindByStringViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var returnObject = _db.Airports
                    .Where(result => result.City.Contains("New Orleans"))
                    .ToList();
                return Json(returnObject);
            }//end if
            return Json(new {success = false});
        }//end method

        [HttpPost]
        [Route("FindByCountry")]
        public JsonResult FindEntryByCountry([FromBody] FindByStringViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var returnObject = _db.Airports
                    .Where(result => result.Country.Contains(token.Content))
                    .ToList();
                return Json(returnObject);
            }//end if
            return Json(new {success = false});
        }//end method

        [HttpPost]
        [Route("FindByDomestic")]
        public JsonResult FindEntryByDomesticIcao([FromBody] FindByStringViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var returnObject = _db.Airports
                    .Where(result => result.DomesticIcao.Contains(token.Content))
                    .ToList();
                return Json(returnObject);
            }//end if
            return Json(new {success = false});
        }//end method

        [HttpPost]
        [Route("FindByInternational")]
        public JsonResult FindEntryByInternationalIcao([FromBody] FindByStringViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var returnObject = _db.Airports
                    .Where(result => result.InternationalIcao.Contains(token.Content))
                    .ToList();
                return Json(returnObject);
            }//end if
            return Json(new {success = false});
        }//end method

        [HttpPost]
        [Route("Delete")]
        public JsonResult DeleteEntry([FromBody] FindByStringViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var foundAirport = _db.Airports.Find(token.Content);
                _db.Airports.Remove(foundAirport);
                _db.SaveChanges();
                return Json(new {success = true});
            }//end if
            return Json(new {success = false});
        }//end method

    }//end class

}//end namespace