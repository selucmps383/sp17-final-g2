﻿using Microsoft.AspNetCore.Mvc;
using YouFlyAPI.Data.DataAccessClasses;
using YouFlyAPI.Data.ViewModels.AccountViewModels;

namespace YouFlyAPI.Controllers.AccountControllers
{//begin namespace

    [Produces("application/json")]
    [Route("api")]
    public class AccountController : Controller
    {//begin class

        private readonly AccountDataAccess _account;

        public AccountController(AccountDataAccess account)
        {//begin constructor
            _account = account;
        }//end constructor

        [HttpPost]
        [Route("register")]
        public JsonResult Register([FromBody] RegisterViewModel input)
        {//begin method
            if (_account.CreateNewUser(input))
            {//begin if
                return Json(new { success = true });
            }//end if
            return Json(new { success = false });
        }//end method

        [HttpPost]
        [Route("login")]
        public JsonResult Login([FromBody] LoginViewModel input)
        {//begin method
            var token = _account.CreaseSessionToken(input);
            if (token.Token.Length > 0) //&& token.User.VerifiedEmail == true
            {//begin if
                return Json(new { success = true, token = token.Token, role = _account.GetRoleById(token.User.RoleId).Description });
            }//end if
            return Json(new { success = false });
        }//end method

    }//end class

}//end namespace