﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using YouFlyAPI.Data;
using YouFlyAPI.Data.DataAccessClasses;
using YouFlyAPI.Data.ViewModels;
using YouFlyAPI.Models;
using YouFlyAPI.Models.CreationModels;

namespace YouFlyAPI.Controllers
{//begin namespace

    [Produces("application/json")]
    [Route("api/[controller]")]
    public class FlightController : Controller
    {//begin class

        private readonly AccountDataAccess _account;
        private readonly DataContext _db;

        public FlightController(DataContext newContext, AccountDataAccess newAccount)
        {//begin constructor
            _db = newContext;
            _account = newAccount;
        }//end constructor

        [HttpPost]
        [Route("GetList")]
        public JsonResult GetAirportList([FromBody] BaseViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var flightList = from flight in _db.Flights
                    select new Flight
                    {//begin define
                        Id = flight.Id,
                        DepartAirport = flight.DepartAirport,
                        DepartAirportId = flight.DepartAirportId,
                        DepartureTime = flight.DepartureTime,
                        ArrivalAirport = flight.ArrivalAirport,
                        ArrivalAirportId = flight.ArrivalAirportId,
                        ArrivalTime = flight.ArrivalTime,
                        Seats = flight.Seats,
                        TravelTime = flight.TravelTime
                    }; //end define
                return Json(flightList.ToList());
            }//end if
            return Json(new { success = false });
        }//end method

        [HttpPost]
        [Route("Create")]
        public JsonResult CreateFlight([FromBody] FlightCreationModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                //create new flight
                var flight = new Flight();
                flight.Name = token.Name;
                flight.FirstClassPrice = token.FirstClassPrice;
                flight.BusinessClassPrice = token.BusinssClassPrice;
                flight.TravelTime = token.TravelTime;

                //create departure airport
                flight.DepartureTime = token.DepartureTime;
                flight.DepartAirportId = token.DepartureAirportId;
                var dp = _db.Airports
                    .Where(result => result.Id.Equals(token.DepartureAirportId))
                    .ToList()
                    .First();
                flight.DepartAirport = dp;

                //create arrival airport
                flight.ArrivalTime = token.ArrivalTime;
                flight.ArrivalAirportId = token.ArrivalAirportId;
                var ap = _db.Airports
                    .Where(result => result.Id.Equals(token.DepartureAirportId))
                    .ToList()
                    .First();
                flight.ArrivalAirport = ap;

                //add to database
                _db.Flights.Add(flight);
                _db.SaveChanges();

                //create seats
                var seatNumber = 1;

                //first class seats.
                var firstClassSeats = new List<Seat>();
                for(int i = 0; i < token.FirstClassSeats; i++)
                {//begin for
                    var newSeat = new Seat();
                    newSeat.SeatNumber = seatNumber;
                    newSeat.SeatSold = false;
                    newSeat.SeatClaimed = false;
                    newSeat.SeatClass = true;
                    newSeat.PurchaseDate = DateTime.MinValue;
                    newSeat.Price = token.FirstClassPrice;
                    newSeat.User = null;
                    newSeat.UserId = 0;
                    newSeat.FlightId = flight.Id;
                    newSeat.Flight = flight;
                    seatNumber++;
                }//end for

                //business class seats
                var businessClasSeats = new List<Seat>();
                for (int i = 0; i < token.BusinessSeats; i++)
                {//begin for
                    var newSeat = new Seat();
                    newSeat.SeatNumber = seatNumber;
                    newSeat.SeatSold = false;
                    newSeat.SeatClaimed = false;
                    newSeat.SeatClass = false;
                    newSeat.PurchaseDate = DateTime.MinValue;
                    newSeat.Price = token.FirstClassPrice;
                    newSeat.User = null;
                    newSeat.UserId = 0;
                    newSeat.FlightId = flight.Id;
                    newSeat.Flight = flight;
                    seatNumber++;
                }//end for

                //add seats to database.
                for (int i = 0; i < firstClassSeats.Count; i++)
                {//begin for
                    _db.Seats.Add(firstClassSeats[i]);
                    _db.SaveChanges();
                }//end for
                for (int i = 0; i < businessClasSeats.Count; i++)
                {//begin for
                    _db.Seats.Add(businessClasSeats[i]);
                    _db.SaveChanges();
                }//end for
                return Json(new {success = true});
            }//end if
            return Json(new { success = false });
        }//end method

        [HttpPost]
        [Route("Edit")]
        public JsonResult PostChangesForEdit([FromBody] FlightCreationModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var flight = _db.Flights.First(result => result.Id.Equals(token.Id));
                flight.Name = token.Name;
                flight.FirstClassPrice = token.FirstClassPrice;
                flight.BusinessClassPrice = token.BusinssClassPrice;
                flight.TravelTime = token.TravelTime;

                //create departure airport
                flight.DepartureTime = token.DepartureTime;
                flight.DepartAirportId = token.DepartureAirportId;
                var dp = _db.Airports
                    .Where(result => result.Id.Equals(token.DepartureAirportId))
                    .ToList()
                    .First();
                flight.DepartAirport = dp;

                //create arrival airport
                flight.ArrivalTime = token.ArrivalTime;
                flight.ArrivalAirportId = token.ArrivalAirportId;
                var ap = _db.Airports
                    .Where(result => result.Id.Equals(token.DepartureAirportId))
                    .ToList()
                    .First();
                flight.ArrivalAirport = ap;

                //add to database
                _db.Entry(flight).State = EntityState.Modified;
                _db.SaveChanges();
                return Json(new { success = true });
            }//end if
            return Json(new { success = false });
        }//end method

        [HttpPost]
        [Route("FindById")]
        public JsonResult FindEntryById([FromBody] FindByIdViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if(tokenCheck)
            {//begin if
                var foundFlightData = _db.Flights.Find(token.Id);
                var flight = new Flight
                {//begin define
                    Id = foundFlightData.Id,
                    DepartAirport = foundFlightData.DepartAirport,
                    DepartAirportId = foundFlightData.DepartAirportId,
                    DepartureTime = foundFlightData.DepartureTime,
                    ArrivalAirport = foundFlightData.ArrivalAirport,
                    ArrivalAirportId = foundFlightData.ArrivalAirportId,
                    ArrivalTime = foundFlightData.ArrivalTime,
                    Seats = foundFlightData.Seats,
                    TravelTime = foundFlightData.TravelTime
                };//end define
                return Json(flight);
            }//end if
            return Json(new { success = false });
        }//end method

        [HttpPost]
        [Route("FindByName")]
        public JsonResult FindEntryByName([FromBody] FindByStringViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var returnObject = _db.Flights
                    .Where(result => result.Name.Equals(token.Content))
                    .ToList();
                return Json(returnObject);
            }//end if
            return Json(new { success = false });
        }//end method

        [HttpPost]
        [Route("FindByDepartureId")]
        public JsonResult FindEntryByDepartureId([FromBody] FindByIdViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var returnObject = _db.Flights
                    .Where(result => result.DepartAirport.Id.Equals(token.Id))
                    .ToList();
                return Json(returnObject);
            }//end if
            return Json(new { success = false });
        }//end method

        [HttpPost]
        [Route("FindByDepartureName")]
        public JsonResult FindEntryByDepartureName([FromBody] FindByStringViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var returnObject = _db.Flights
                    .Where(result => result.DepartAirport.Name.Contains(token.Content))
                    .ToList();
                return Json(returnObject);
            }//end if
            return Json(new { success = false });
        }//end method

        [HttpPost]
        [Route("FindByDepartureCity")]
        public JsonResult FindEntryByDepartureCity([FromBody] FindByStringViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var returnObject = _db.Flights
                    .Where(result => result.DepartAirport.City.Contains(token.Content))
                    .ToList();
                return Json(returnObject);
            }//end if
            return Json(new { success = false });
        }//end method

        [HttpPost]
        [Route("FindByDepartureCountry")]
        public JsonResult FindEntryByDepartureCountry([FromBody] FindByStringViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var returnObject = _db.Flights
                    .Where(result => result.DepartAirport.Country.Contains(token.Content))
                    .ToList();
                return Json(returnObject);
            }//end if
            return Json(new { success = false });
        }//end method

        [HttpPost]
        [Route("FindByArrivalId")]
        public JsonResult FindEntryByArrivalId([FromBody] FindByIdViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var returnObject = _db.Flights
                    .Where(result => result.ArrivalAirport.Id.Equals(token.Id))
                    .ToList();
                return Json(returnObject);
            }//end if
            return Json(new { success = false });
        }//end method

        [HttpPost]
        [Route("FindByArrivalName")]
        public JsonResult FindEntryByArrivalName([FromBody] FindByStringViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var returnObject = _db.Flights
                    .Where(result => result.ArrivalAirport.Name.Contains(token.Content))
                    .ToList();
                return Json(returnObject);
            }//end if
            return Json(new { success = false });
        }//end method

        [HttpPost]
        [Route("FindByArrivalCity")]
        public JsonResult FindEntryByArrivalCity([FromBody] FindByStringViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var returnObject = _db.Flights
                    .Where(result => result.ArrivalAirport.City.Contains(token.Content))
                    .ToList();
                return Json(returnObject);
            }//end if
            return Json(new { success = false });
        }//end method

        [HttpPost]
        [Route("FindByArrivalCountry")]
        public JsonResult FindEntryBArrivalCountry([FromBody] FindByStringViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var returnObject = _db.Flights
                    .Where(result => result.ArrivalAirport.Country.Contains(token.Content))
                    .ToList();
                return Json(returnObject);
            }//end if
            return Json(new { success = false });
        }//end method

        [HttpPost]
        [Route("Delete")]
        public JsonResult DeleteEntry([FromBody] FindByIdViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var foundFlight = _db.Airports.Find(token.Id);
                _db.Airports.Remove(foundFlight);
                _db.SaveChanges();
                return Json(new { success = true });
            }//end if
            return Json(new { success = false });
        }//end method

        private Airport FindAirport(int newId)
        {//begin method
            var foundAirportData = _db.Airports.Find(newId);
            var airport = new Airport
            {//begin define
                Id = foundAirportData.Id,
                Name = foundAirportData.Name,
                City = foundAirportData.City,
                Country = foundAirportData.Country,
                DomesticIcao = foundAirportData.Country,
                InternationalIcao = foundAirportData.InternationalIcao,
                Lattitude = foundAirportData.Lattitude,
                Longitude = foundAirportData.Longitude,
                Altitude = foundAirportData.Altitude,
                Timezone = foundAirportData.Timezone,
            }; //end define
            return airport;
        }//end method

    }//end class

}//end namespace
