﻿

using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using YouFlyAPI.Data;
using YouFlyAPI.Data.DataAccessClasses;
using YouFlyAPI.Models;
using YouFlyAPI.Models.AccountModels;
using YouFlyAPI.Tools;

namespace YouFlyAPI.Controllers
{//begin namespace

    [Produces("application/json")]
    [Route("api/tester")]
    public class TestController : Controller
    {//begin class

        private readonly AccountDataAccess _account;
        private readonly DataContext _db;

        public TestController(DataContext context, AccountDataAccess newAccount)
        {//begin constructor
            _db = context;
            _account = newAccount;
        }//end constructor

        [HttpGet]
        [Route("getairports")]
        public JsonResult GetAirportList()
        {//begin method
            //var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            //if(tokenCheck)
            //{//begin if
            var airportList = from airport in _db.Airports
                select new Airport
                {//begin define
                    Id = airport.Id,
                    Name = airport.Name,
                    City = airport.City,
                    Country = airport.Country,
                    DomesticIcao = airport.DomesticIcao,
                    InternationalIcao = airport.InternationalIcao,
                    Lattitude = airport.Lattitude,
                    Longitude = airport.Longitude,
                    Altitude = airport.Altitude,
                    Timezone = airport.Timezone,
                }; //end define
            //var returnObject = Json(airportList.ToList());
            return Json(airportList.ToList());
            //}//end if
        }//end method

        [HttpGet]
        [Route("getusers")]
        public JsonResult GetUsers()
        {//begin method
            var userList = from user in _db.Users
                select new UserModel
                {//begin define
                    Id = user.Id,
                    Email = user.Email,
                    Password = user.Password,
                    Role = user.Role,
                    RoleId = user.RoleId,
                    VerifiedEmail = user.VerifiedEmail,
                }; //end define
            return Json(userList.ToList());
        }//end method

        [HttpGet]
        [Route("getflights")]
        public JsonResult GetFlightList()
        {//begin method
            var flightList = from flight in _db.Flights
                select new Flight
                {//begin define
                    Id = flight.Id,
                    DepartAirport = flight.DepartAirport,
                    DepartAirportId = flight.DepartAirportId,
                    DepartureTime = flight.DepartureTime,
                    ArrivalAirport = flight.ArrivalAirport,
                    ArrivalAirportId = flight.ArrivalAirportId,
                    ArrivalTime = flight.ArrivalTime,
                    Seats = flight.Seats,
                    TravelTime = flight.TravelTime
                }; //end define
            return Json(flightList.ToList());
        }//end method

        [HttpGet]
        [Route("createFlight")]
        public JsonResult CreateFlight()
        {//begin method
            var flight = new Flight();
            flight.Name = "YF01";
            flight.FirstClassPrice = 500.00;
            flight.BusinessClassPrice = 350.00;
            flight.TravelTime = 3.5;

            //create departure airport
            flight.DepartureTime = DateTime.Today;
            flight.DepartAirportId = 1;
            var dp = _db.Airports
                .Where(result => result.Id.Equals(flight.DepartAirportId))
                .ToList()
                .First();
            flight.DepartAirport = dp;

            //create arrival airport
            flight.ArrivalTime = DateTime.Now;
            flight.ArrivalAirportId = 9;
            var ap = _db.Airports
                .Where(result => result.Id.Equals(flight.ArrivalAirportId))
                .ToList()
                .First();
            flight.ArrivalAirport = ap;

            //add to database
            _db.Flights.Add(flight);
            _db.SaveChanges();
            return Json(flight);
        }//end method























    }//end class

}//end class
