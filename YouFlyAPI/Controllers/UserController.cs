﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YouFlyAPI.Data;
using YouFlyAPI.Data.DataAccessClasses;
using YouFlyAPI.Data.ViewModels;
using YouFlyAPI.Models.AccountModels;

namespace YouFlyAPI.Controllers
{//begin namespace

    [Produces("application/json")]
    [Route("api/users")]
    public class UserController : Controller
    {//begin class

        private readonly AccountDataAccess _account;
        private readonly DataContext _db;

        public UserController(DataContext context, AccountDataAccess newAccount)
        {//begin constructor
            _db = context;
            _account = newAccount;
        }//end constructor

        [HttpPost]
        [Route("getusers")]
        public JsonResult GetUsers([FromBody] BaseViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var userList = from user in _db.Users
                    select new UserModel
                    {//begin define
                        Id = user.Id,
                        Email = user.Email,
                        Password = user.Password,
                        Role = user.Role,
                        RoleId = user.RoleId,
                        VerifiedEmail = user.VerifiedEmail,
                    }; //end define
                return Json(userList.ToList());
            }//end if
            return Json(new {success = false});
        }//end method

        [HttpPost]
        [Route("GetUserByEmail")]
        public JsonResult GetUserByEmail([FromBody] FindByStringViewModel token)
        {//begin method
            var tokenCheck = _account.CheckSessionToken(token.SessionToken);
            if (tokenCheck)
            {//begin if
                var returnObject = _db.Users
                    .Where(result => result.Email.Contains(token.Content));
                return Json(returnObject);
            }//end if
            return Json(new {success = false});
        }//end method

    }//end class

}//end namespace
