﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using YouFlyAPI.Data.DataAccessClasses;
using YouFlyAPI.Data.ViewModels;

namespace YouFlyAPI.Controllers
{//begin namespace

    [Produces("application/json")]
    [Route("api")]
    public class OrderController : Controller
    {//begin class

        private readonly AccountDataAccess _account;
        private readonly OrdersDataAccess _orders;

        public OrderController(AccountDataAccess account, OrdersDataAccess orders)
        {//begin constructor
            _orders = orders;
            _account = account;
        }//end constructor

        [HttpGet]
        public IEnumerable<string> Get()
        {//begin method
            return new string[] { "value1", "value2" };
        }//end method

    }//end class

}//end namespace