﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using YouFlyAPI.Data;

namespace YouFlyAPI.Models.AccountModels
{//begin namespace

    public class UserModel : BaseDataModel
    {//begin class

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public bool VerifiedEmail { get; set; }
        public byte[] Salt { get; set; }

        [ForeignKey("Role")]
        public int RoleId { get; set; }
        public RoleModel Role { get; set; }

        public ICollection<SessionModel> Sessions { get; set; }

        public ICollection<Seat> Seats { get; set; }

    }//end class

}//end namespace