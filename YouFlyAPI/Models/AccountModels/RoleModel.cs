﻿using System.Collections.Generic;
using YouFlyAPI.Data;

namespace YouFlyAPI.Models.AccountModels
{//begin namespace

    public class RoleModel : BaseDataModel
    {//begin class

        public int Rank { get; set; }
        public string Description { get; set; }
        public ICollection<UserModel> Users { get; set; }

    }//end class

}//end namespace