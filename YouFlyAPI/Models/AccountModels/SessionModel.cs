﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using YouFlyAPI.Data;

namespace YouFlyAPI.Models.AccountModels
{//begin namespace

    public class SessionModel : BaseDataModel
    {//begin class

        [ForeignKey("User")]
        public int UserId { get; set; }
        public string Token { get; set; }
        public DateTime ExpireDateTime { get; set; }
        public UserModel User { get; set; }

    }//end class

}//end namespace