﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using YouFlyAPI.Data;

namespace YouFlyAPI.Models
{//begin namespace

    public class Flight : BaseDataModel
    {//begin class

        public string Name { get; set; }
        public DateTime DepartureTime { get; set; }
        public DateTime ArrivalTime { get; set; }
        public double TravelTime { get; set; }
        public double FirstClassPrice { get; set; }
        public double BusinessClassPrice { get; set; }

        public ICollection<Seat> Seats { get; set; }

        [ForeignKey("DepartAirport")]
        public int? DepartAirportId { get; set; }
        public Airport DepartAirport { get; set; }

        [ForeignKey("ArrivalAirport")]
        public int? ArrivalAirportId { get; set; }
        public Airport ArrivalAirport { get; set; }

    }//end class

}//end namespace
