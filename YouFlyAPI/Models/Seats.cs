﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;
using YouFlyAPI.Data;
using YouFlyAPI.Models.AccountModels;

namespace YouFlyAPI.Models
{//begin namespace

    public class Seat : BaseDataModel
    {//begin class

        public int SeatNumber { get; set; }
        public bool SeatSold { get; set; }
        public bool SeatClaimed { get; set; }
        public bool SeatClass { get; set; }
        public DateTime PurchaseDate { get; set; }
        public double Price { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        public UserModel User { get; set; }

        [ForeignKey("Flight")]
        public int FlightId { get; set; }
        public Flight Flight { get; set; }

    }//end class

}//end bamespace
