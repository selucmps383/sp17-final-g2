﻿
using System;
using YouFlyAPI.Data.ViewModels;

namespace YouFlyAPI.Models.CreationModels
{//begin namespace

    public class FlightCreationModel : BaseViewModel
    {//begin class

        public int Id { get; set; }
        public string Name { get; set; }
        public double FirstClassPrice { get; set; }
        public double BusinssClassPrice { get; set; }
        public int DepartureAirportId { get; set; }
        public int ArrivalAirportId { get; set; }
        public DateTime DepartureTime { get; set; }
        public DateTime ArrivalTime { get; set; }
        public double TravelTime { get; set; }
        public int TravelDistance { get; set; }
        public int FirstClassSeats { get; set; }
        public int BusinessSeats { get; set; }

    }//end class

}//end namespace
