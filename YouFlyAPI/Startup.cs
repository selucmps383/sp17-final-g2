﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using System.IO;
using YouFlyAPI.Data;
using YouFlyAPI.Data.DataAccessClasses;

namespace YouFlyAPI
{//begin namespace

    public class Startup
    {//begin class

        public IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment env)
        {//begin constructor
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }//end constructor

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {//begin method
            // Add framework services.
            var connection = @"Server=tcp:youfly.database.windows.net,1433;Initial Catalog=youflyapi;Persist Security Info=False;User ID=mdhutto;Password=Abc00cba;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            services.AddDbContext<DataContext>(options => options.UseSqlServer(connection));
            services.AddScoped<AccountDataAccess>();
            services.AddMvc();
            services.AddCors(o => o.AddPolicy("AllowAll", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
            }));
            services.AddCors(o => o.AddPolicy("allowLiveSites", builder =>
            {
                builder.WithOrigins("http://blah.com")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
            }));
            services.AddCors(o => o.AddPolicy("localHost", builder =>
            {
                builder.WithOrigins("http://localhost:4200")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
            }));
        }//end method


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, DataContext context)
        {//begin method
            app.UseCors("localHost");
            app.UseCors("AllowAll");
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            app.Use(async (routecontext, next) =>
                {
                    await next();
                    if (routecontext.Response.StatusCode == 404 && !Path.HasExtension(routecontext.Request.Path.Value))
                    {
                        routecontext.Request.Path = "/index.html";
                        await next();
                    }
                })
                .UseDefaultFiles(new DefaultFilesOptions { DefaultFileNames = new List<string> { "index.html" } })
                .UseStaticFiles()
                .UseMvc();
            var account = new AccountDataAccess(context);
            var init = new DbInitializer(account);
            init.Initialize(context);
        }//end method

    }//end class

}//end namespace;
