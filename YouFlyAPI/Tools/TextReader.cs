﻿
using System.IO;

namespace YouFlyAPI.Tools
{//begin namespace

    internal class TextReader
    {//begin class

        private const string Filename = "c:\\users\\michaelhutto\\documents\\airports.txt";

        public string[] GetTextArray()
        {//begin method
            return File.ReadAllLines(Filename);
        }//end method

    }//end class

}//end namespace