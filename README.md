## Overview

In this phase, we will be creating a solution for a fledgling domestic airline. They have a small fleet
of planes and contracts with existing airports in place, but no web presence at all.

## Installation and Technology

In this phase, we will be using Node.JS, Angular 2 and CLI, VS Code, Xamarin, and Visual Studio 2017.
We will have a customer facing website using the ASP.NET Core Web API. The mobile application will be made with Xamarin. We will also be using the SQL Server database to manage the accounts and information.

## Important Information

There will be a code freeze before final presentations, and we will be presenting at random on May 2, 2017. Presentations are limited to 10 minutes and 5 minutes of questions.