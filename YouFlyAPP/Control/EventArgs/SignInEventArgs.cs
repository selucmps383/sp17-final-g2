﻿
/*****************************************************************************/
/*                                                                           */
/* YouFly - Group 2 Final Phase                                              */
/* Date: 3/29/2017                                                           */
/* Author:  Michael Hutto                                                    */
/* LogInFragment                                                             */
/*                                                                           */
/*****************************************************************************/

using System;

namespace YouFly
{//begin namepsace
	
	public class SignInEventArgs : EventArgs
	{//begin class

		private string email;
		private string password;

		public void setEmail(string newEmail)
		{//begin method
			email = newEmail;
		}//end method

		public void setPassword(string newPassword)
		{//begin method
			password = newPassword;
		}//end method

		public string getEmail()
		{//begin method
			return email;
		}//end method

		public string getpasword()
		{//begin method
			return password;
		}//end method

	}//end class

}//end namespace
