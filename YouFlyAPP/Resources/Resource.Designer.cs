#pragma warning disable 1591
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.42000
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

[assembly: Android.Runtime.ResourceDesignerAttribute("YouFly.Resource", IsApplication=true)]

namespace YouFly
{
	
	
	[System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Android.Build.Tasks", "1.0.0.0")]
	public partial class Resource
	{
		
		static Resource()
		{
			global::Android.Runtime.ResourceIdManager.UpdateIdValues();
		}
		
		public static void UpdateIdValues()
		{
			global::ZXing.Mobile.Resource.Id.contentFrame = global::YouFly.Resource.Id.contentFrame;
			global::ZXing.Mobile.Resource.Layout.zxingscanneractivitylayout = global::YouFly.Resource.Layout.zxingscanneractivitylayout;
			global::ZXing.Mobile.Resource.Layout.zxingscannerfragmentlayout = global::YouFly.Resource.Layout.zxingscannerfragmentlayout;
		}
		
		public partial class Animation
		{
			
			// aapt resource value: 0x7f050000
			public const int slideRight = 2131034112;
			
			// aapt resource value: 0x7f050001
			public const int slideUp = 2131034113;
			
			static Animation()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Animation()
			{
			}
		}
		
		public partial class Attribute
		{
			
			static Attribute()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Attribute()
			{
			}
		}
		
		public partial class Drawable
		{
			
			// aapt resource value: 0x7f020000
			public const int flight = 2130837504;
			
			// aapt resource value: 0x7f020001
			public const int login = 2130837505;
			
			// aapt resource value: 0x7f020002
			public const int youFLYblue2 = 2130837506;
			
			static Drawable()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Drawable()
			{
			}
		}
		
		public partial class Id
		{
			
			// aapt resource value: 0x7f08000d
			public const int btnLeft = 2131230733;
			
			// aapt resource value: 0x7f080005
			public const int btnLogIn = 2131230725;
			
			// aapt resource value: 0x7f080010
			public const int btnQRCodes = 2131230736;
			
			// aapt resource value: 0x7f08000e
			public const int btnRight = 2131230734;
			
			// aapt resource value: 0x7f080003
			public const int btnSignIn = 2131230723;
			
			// aapt resource value: 0x7f080011
			public const int contentFrame = 2131230737;
			
			// aapt resource value: 0x7f080004
			public const int imgLogo = 2131230724;
			
			// aapt resource value: 0x7f080009
			public const int imgQRImage = 2131230729;
			
			// aapt resource value: 0x7f080000
			public const int imgSignIn = 2131230720;
			
			// aapt resource value: 0x7f080006
			public const int pgsLogIn = 2131230726;
			
			// aapt resource value: 0x7f080007
			public const int txtAcioInfo = 2131230727;
			
			// aapt resource value: 0x7f080008
			public const int txtCityStateInfo = 2131230728;
			
			// aapt resource value: 0x7f080001
			public const int txtEmail = 2131230721;
			
			// aapt resource value: 0x7f08000c
			public const int txtFlightNumber = 2131230732;
			
			// aapt resource value: 0x7f08000a
			public const int txtFlightTime = 2131230730;
			
			// aapt resource value: 0x7f08000b
			public const int txtPassenger = 2131230731;
			
			// aapt resource value: 0x7f080002
			public const int txtPassword = 2131230722;
			
			// aapt resource value: 0x7f08000f
			public const int txtUser = 2131230735;
			
			static Id()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Id()
			{
			}
		}
		
		public partial class Layout
		{
			
			// aapt resource value: 0x7f040000
			public const int LoginLayout = 2130968576;
			
			// aapt resource value: 0x7f040001
			public const int Main = 2130968577;
			
			// aapt resource value: 0x7f040002
			public const int PurchaseDetailsListView = 2130968578;
			
			// aapt resource value: 0x7f040003
			public const int QRCodeLayout = 2130968579;
			
			// aapt resource value: 0x7f040004
			public const int UserDetails = 2130968580;
			
			// aapt resource value: 0x7f040005
			public const int zxingscanneractivitylayout = 2130968581;
			
			// aapt resource value: 0x7f040006
			public const int zxingscannerfragmentlayout = 2130968582;
			
			static Layout()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Layout()
			{
			}
		}
		
		public partial class Mipmap
		{
			
			// aapt resource value: 0x7f030000
			public const int flight = 2130903040;
			
			// aapt resource value: 0x7f030001
			public const int youFLYblue2 = 2130903041;
			
			static Mipmap()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Mipmap()
			{
			}
		}
		
		public partial class String
		{
			
			// aapt resource value: 0x7f060001
			public const int app_name = 2131099649;
			
			// aapt resource value: 0x7f060000
			public const int hello = 2131099648;
			
			static String()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private String()
			{
			}
		}
		
		public partial class Style
		{
			
			// aapt resource value: 0x7f070000
			public const int dialog_animation = 2131165184;
			
			static Style()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Style()
			{
			}
		}
	}
}
#pragma warning restore 1591
