﻿
/*****************************************************************************/
/*                                                                           */
/* YouFly - Group 2 Final Phase                                              */
/* Date: 3/23/2017                                                           */
/* Author:  Michael Hutto                                                    */
/* MainActivity                                                              */
/*                                                                           */
/*****************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace YouFly
{//begin namespace
	
	[Activity(Label = "UserDetailsActivity")]
	public class UserDetailsActivity : Activity
	{//begin class

		//MSY,COS,2017/3/13,17:30,Matthew,Person,FirstClass,YF#1234567

		private Button btnQRCodes;


		protected override void OnCreate(Bundle savedInstanceState)
		{//begin method
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.UserDetails);
			btnQRCodes = FindViewById<Button>(Resource.Id.btnQRCodes);
			btnQRCodes.Click += viewQRCodes;
		}//end method

		void viewQRCodes(object sender, EventArgs e)
		{//begin method
			StartActivity(typeof(QRActivity));
		}//end method

}//end class

}//end namespace
