﻿
/*****************************************************************************/
/*                                                                           */
/* YouFly - Group 2 Final Phase                                              */
/* Date: 3/23/2017                                                           */
/* Author:  Michael Hutto                                                    */
/* MainActivity                                                              */
/*                                                                           */
/*****************************************************************************/

using System;
using Android.App;
using Android.OS;
using Android.Widget;

namespace YouFly
{//begin namespace
	
	[Activity(Label = "YouFly", MainLauncher = true, Icon = "@mipmap/flight")]
	public class MainActivity : Activity
	{//begin class
		
		private Button btnLogIn;
		private EditText txtEmail;
		private EditText txtPassword;
		private ProgressBar pgsLogin;

		protected override void OnCreate(Bundle savedInstanceState)
		{//begin method
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.Main);
			btnLogIn = FindViewById<Button>(Resource.Id.btnLogIn);
			txtEmail = FindViewById<EditText>(Resource.Id.txtEmail);
			txtPassword = FindViewById<EditText>(Resource.Id.txtPassword);
			pgsLogin = FindViewById<ProgressBar>(Resource.Id.pgsLogIn);
			btnLogIn.Click += login;
		}//end method

		void login(object sender, EventArgs e)
		{//begin method
			if (txtEmail.Text == "michael.hutto@selu.edu" && txtPassword.Text == "pass")
			{//begin if
				btnLogIn.Text = "Logging In...";
				pgsLogin.Visibility = Android.Views.ViewStates.Visible;
				btnLogIn.Text = "Successful...";
                StartActivity(typeof(UserDetailsActivity));

			}//end if
			else
			{//begin else
				btnLogIn.Text = "Login Unsuccessful";
			}//end else
		}//end method

}//end class

}//end namespace
