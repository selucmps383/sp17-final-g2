﻿
/*****************************************************************************/
/*                                                                           */
/* YouFly - Group 2 Final Phase                                              */
/* Date: 3/23/2017                                                           */
/* Author:  Michael Hutto                                                    */
/* QRGenerator                                                               */
/*                                                                           */
/*****************************************************************************/

using Android.Graphics;

namespace YouFly
{//begin namespace
	
	public class QRGenerator
	{//begin class

		private readonly Bitmap code;
		
		public QRGenerator(string newCSV)
		{//begin constructor
			var barcodeWriter = new ZXing.Mobile.BarcodeWriter
			{//begin declare
				Format = ZXing.BarcodeFormat.QR_CODE,
				Options = new ZXing.Common.EncodingOptions
				{//begin declare
					Width = 300,
					Height = 300
				}//end declare
			};//end declare
			code = barcodeWriter.Write(newCSV);
		}//end constructor

		public Bitmap getCode()
		{//begin method
			return code;
		}//end method

	}//end class

}//end namespace
