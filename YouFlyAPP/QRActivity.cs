﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace YouFly
{//begin namespace
	
	[Activity(Label = "QRActivity")]
	public class QRActivity : Activity
	{//begin class

		private TextView txtAcioInfo;
		private TextView txtCityStateInfo;
		private ImageView imgQRImage;
		private TextView txtFlightTime;
		private TextView txtPassenger;
		private TextView txtFlightNumber;
		private Button btnLeft;
		private Button btnRight;

		private int currentView = 0;
		
		protected override void OnCreate(Bundle savedInstanceState)
		{//begin method
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.QRCodeLayout);
			txtAcioInfo = FindViewById<TextView>(Resource.Id.txtAcioInfo);
			txtCityStateInfo = FindViewById<TextView>(Resource.Id.txtCityStateInfo);
			imgQRImage = FindViewById<ImageView>(Resource.Id.imgQRImage);
			txtFlightTime = FindViewById<TextView>(Resource.Id.txtFlightTime);
			txtPassenger = FindViewById<TextView>(Resource.Id.txtPassenger);
			txtFlightNumber = FindViewById<TextView>(Resource.Id.txtFlightNumber);
			btnLeft = FindViewById<Button>(Resource.Id.btnLeft);
			btnRight = FindViewById<Button>(Resource.Id.btnRight);
			btnLeft.Click += goLeft;
			btnRight.Click += goRight;
			btnLeft.Clickable = false;

			updateText(currentView);




		}//end method

		void goLeft(object sender, EventArgs e)
		{//beging method
			currentView--;
			updateText(currentView);
		}//end method

		void goRight(object sender, EventArgs e)
		{//begin method
			currentView++;
			updateText(currentView);
		}//end method

		void updateText(int newVal)
		{//begin method
			if (currentView == 0)
			{//begin if
				btnLeft.Clickable = false;
				btnRight.Clickable = true;
			}//end if
			if (currentView == 1)
			{//begin if
				btnLeft.Clickable = true;
				btnRight.Clickable = true;
			}//end if
			if (currentView == 2)
			{//begin if
				btnLeft.Clickable = true;
				btnRight.Clickable = false;
			}//end if
			var view = new SeatView();
			var seatList = view.getList();
			var seat = seatList[currentView];
			txtAcioInfo.Text = seat.departIcao + " to " + seat.arrivalIcao;
			txtCityStateInfo.Text = seat.departCity + " to " + seat.arriveCity;
			txtFlightTime.Text = "8 Hours";
			txtPassenger.Text = seat.firstName + " " + seat.lastName;
			txtFlightNumber.Text = seat.flightName;
			var qImage = new QRGenerator(seat.qString);
			imgQRImage.SetImageBitmap(qImage.getCode());
		}//end method














}//end class

}//end namespace
