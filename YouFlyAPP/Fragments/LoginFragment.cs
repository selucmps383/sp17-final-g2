﻿
/*****************************************************************************/
/*                                                                           */
/* YouFly - Group 2 Final Phase                                              */
/* Date: 3/23/2017                                                           */
/* Author:  Michael Hutto                                                    */
/* LogInFragment                                                             */
/*                                                                           */
/*****************************************************************************/

using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace YouFly
{//begin namespace
	
	public class LogInFragment : DialogFragment
	{//begin class

		private EditText txtEmail;
		private EditText txtPassword;
		private Button btnSignIn;
		public event EventHandler<SignInEventArgs> signInEvent;

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{//begin method
			base.OnCreateView(inflater, container, savedInstanceState);
			var view = inflater.Inflate(Resource.Layout.LoginLayout, container, false);
			txtEmail = view.FindViewById<EditText>(Resource.Id.txtEmail);
			txtPassword = view.FindViewById<EditText>(Resource.Id.txtPassword);
			btnSignIn = view.FindViewById<Button>(Resource.Id.btnSignIn);
			btnSignIn.Click += signIn;
			return view;
		}//end method

		public override void OnActivityCreated(Bundle savedInstanceState)
		{//begin method
			Dialog.Window.RequestFeature(WindowFeatures.NoTitle);
			base.OnActivityCreated(savedInstanceState);
		}//end method

		public void signIn(object sender, EventArgs e)
		{//begin method
			


		}//end method

	}//end class

}//end namespace
