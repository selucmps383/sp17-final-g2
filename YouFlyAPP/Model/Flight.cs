﻿
using System;

namespace YouFly
{//begin namespace
	
	public class Flight
	{//begin class

		public string Name { get; set; }
		public DateTime DepartureTime { get; set; }
		public DateTime ArrivalTime { get; set; }
		public double TravelTime { get; set; }
		public double FirstClassPrice { get; set; }
		public double BusinessClassPrice { get; set; }
		public int DepartAirportId { get; set; }
		public Airport DepartAirport { get; set; }

		public int ArrivalAirportId { get; set; }
		public Airport ArrivalAirport { get; set; }

		
		public Flight()
		{//begin constructor
			//todo stuff
		}//end constructor

	}//end class

}//end namespace
