﻿
using System;
using System.Collections.Generic;

namespace YouFly
{//begin namespace

	public class SeatView
	{//begin class

		//MSY,AMS,2017/5/10,17:30,Michael,Person,FirstClass,YF#1234567
		public string departIcao { get; set; }
		public string arrivalIcao { get; set; }
		public string year { get; set; }
		public string month { get; set; }
		public string day { get; set; }
		public string time { get; set; }
		public string departCity { get; set; }
		public string arriveCity { get; set; }
		public string firstName { get; set; }
		public string lastName { get; set; }
		public string seatClass { get; set; }
		public string flightName { get; set; }
		public string qString { get; set; }


		public List<SeatView> getList()
		{//begin method
			var returnList = new List<SeatView>();
			var mikeSeat = new SeatView();
			mikeSeat.departIcao = "MSY";
			mikeSeat.arrivalIcao = "AMS";
			mikeSeat.departCity = "New Orleans";
			mikeSeat.arriveCity = "Amsterdam";
			mikeSeat.year = "2017";
			mikeSeat.month = "5";
			mikeSeat.day = "10";
			mikeSeat.time = "18:00";
			mikeSeat.firstName = "Michael";
			mikeSeat.lastName = "Hutto";
			mikeSeat.seatClass = "FirstClass";
			mikeSeat.flightName = "YF#6578";
			mikeSeat.qString = "MSY,AMS,2017,5,10,18:00,Michael,Hutto,FirstClass,YF#6578";
			returnList.Add(mikeSeat);
			var clintonSeat = new SeatView();
			clintonSeat.departIcao = "MSY";
			clintonSeat.arrivalIcao = "JFK";
			clintonSeat.year = "2017";
			clintonSeat.departCity = "New Orleans";
			clintonSeat.arriveCity = "New York";
			clintonSeat.month = "5";
			clintonSeat.day = "28";
			clintonSeat.time = "7:00";
			clintonSeat.firstName = "Clinton";
			clintonSeat.lastName = "Walker";
			clintonSeat.seatClass = "FirstClass";
			clintonSeat.flightName = "YF#5686";
			clintonSeat.qString = "MSY,AMS,2017,5,10,18:00,Michael,Hutto,FirstClass,YF#6578";
			returnList.Add(clintonSeat);
			var joshSeat = new SeatView();
			joshSeat.departIcao = "MSY";
			joshSeat.arrivalIcao = "CDG";
			joshSeat.departCity = "New Orleans";
			joshSeat.arriveCity = "Paris";
			joshSeat.year = "2017";
			joshSeat.month = "5";
			joshSeat.day = "18";
			joshSeat.time = "2:00";
			joshSeat.firstName = "Josh";
			joshSeat.lastName = "Wetzel";
			joshSeat.seatClass = "FirstClass";
			joshSeat.flightName = "YF#179867";
			joshSeat.qString = "MSY,AMS,2017,5,10,18:00,Michael,Hutto,FirstClass,YF#6578";
			returnList.Add(joshSeat);
			return returnList;
		}//end method

	}//end class

}//end namespace

