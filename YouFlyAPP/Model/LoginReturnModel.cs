﻿
using System;

namespace YouFly
{//begin namespace
	
	public class LoginReturnModel
	{//begin class

		public bool succes { get; set; }
		public string token { get; set; }
		public string role { get; set; }

	}//end class

}//end namespace
