﻿
using System;

namespace YouFly
{//begin namespace

	public class Seat
	{//begin class

		public string User { get; set; }
		public int SeatNumber { get; set; }
		public bool SeatSold { get; set; }
		public bool SeatClaimed { get; set; }
		public bool SeatClass { get; set; }
		public DateTime PurchaseDate { get; set; }
		public double Price { get; set; }
		public int UserId { get; set; }
		public int FlightId { get; set; }
		public Flight Flight { get; set; }


		public Seat()
		{//begin constructor
			//todo stuff
		}//end constructor

	}//end class

}//end namespace
