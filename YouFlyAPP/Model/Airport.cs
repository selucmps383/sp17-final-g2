﻿
using System;

namespace YouFly
{//begin namespace
	
	public class Airport
	{//begin class

		public string Name { get; set; }
		public string City { get; set; }
		public string Country { get; set; }
		public string DomesticIcao { get; set; }
		public string InternationalIcao { get; set; }
		public double Lattitude { get; set; }
		public double Longitude { get; set; }
		public double Altitude { get; set; }
		public int Timezone { get; set; }
		
		public Airport()
		{//begin constructor
			//todo stuff
		}//end constructor

	}//end class

}//end namespace
