﻿
using System;
using System.Collections.Generic;

namespace YouFly
{//begin namespace
	
	public class UserModel
	{//begin class

		public string Email { get; set; }
		public string Password { get; set; }
		public bool VerifiedEmail { get; set; }
		public byte[] Salt { get; set; }
		public int RoleId { get; set; }

		public List<Seat> Seats { get; set; }
		
		public UserModel()
		{//begin constructor
			//todo stuff
		}//end constructor

	}//end class

}//end namespace
